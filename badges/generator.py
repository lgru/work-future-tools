import sys
import os
import csv
from django.template import Template, Context
from django.template import loader
from django.conf import settings


with open('buttons_mlp.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    #for row in reader:
        #firstname = row[1]
        #lastname = row[2]
        #project = row[3]

        #print """
            #firstname: %(firstname)s
            #lastname: %(lastname)s
            #project: %(project)s
        #""" % locals()

    PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
    settings.configure(
        TEMPLATE_DIRS=(os.path.join(PROJECT_DIR, 'templates'),),
        INSTALLED_APPS=('mathfilters',)
    )
    t = loader.get_template('badges.sla')
    c = Context({
        'PAGEYPOS': 20,
        'SIZE': 174.330708661417,
        'rows': list(reader)
    })

    #sys.stdout.write(t.render(c))
    import codecs
    f = codecs.open('/tmp/foobar.sla', "w", encoding="utf-8")
    f.write(t.render(c))
    f.close()
